
#ifndef SPACE_INVADERS_GAME
#define SPACE_INVADERS_GAME

#include <osbind.h>
#include <stdio.h>
#include "model.h"
#include "event.h"
#include "render.h"
#include "psg.h"
#include "music.h"
#include "game.h"
#include "clear_qk.h"
#include "clear_ga.h"

void start();

#endif /* SPACE_INVADERS_GAME */