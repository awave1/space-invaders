/*
 *  File: clear_ga.h 
 *  Authors: Artem Golovin, Daniel Artuso 
 */

#ifndef CLEAR_GAME_H
#define CLEAR_GAME_H

/**
 * Clear the entire screen. 
 * 
 * @param base - Screen to clear
 */
void clear_game(void *base);

#endif /* CLEAR_QK_H_GAME */